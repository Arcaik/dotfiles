#
# ~/.tmux.conf
#

#
# Main settings
#

set -g default-terminal "screen-256color"
set -g xterm-keys on

set -g repeat-time 0
set -g escape-time 0
set -g set-titles off
set -g bell-action any
set -g display-time 10000

#
# Key binding
#

set -g prefix C-a
setw -g mode-keys vi

unbind C-z

bind c new-window
bind C-c new-window

bind n next-window
bind C-n next-window

bind p previous-window
bind C-p previous-window

bind a last-window
bind C-a last-window

bind h split-window -h
bind C-h split-window -h

bind v split-window -v
bind C-v split-window -v

bind d confirm -p "detach? (y/n)" detach
bind C-d confirm -p "detach? (y/n)" detach

bind x kill-pane
bind C-x kill-pane

bind k confirm -p "kill window? (y/n)" kill-window
bind C-k confirm -p "kill window? (y/n)" kill-window

#
# Window style
#

setw -g monitor-activity off
setw -g automatic-rename on

setw -g window-status-style fg=brightred,bg=default
setw -g window-status-current-style fg=red,bg=default
setw -g pane-border-style fg=black
setw -g pane-active-border-style fg=brightgreen

#
# Status and message bar style
#

set -g status-style fg=yellow,bg=default
set -g message-style fg=red,bg=default

set -g status-left "#[fg=blue][#[fg=default]#H#[fg=blue]]--["
set -g status-left-length 50

set -g status-right "#[fg=blue]]--(#[fg=default]#S#[fg=blue])"
set -g status-right-length 50

set -g status-justify centre
