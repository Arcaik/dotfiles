if [ ! -x "$(which ansible)" ]
then
  return
fi

export ANSIBLE_CONFIG="~/.ansible/ansible.cfg"

_hosts=($(ansible all --list-hosts 2>/dev/null | grep -v "hosts ([0-9]\+)"))

if [ -n "${_hosts}" ]
then
  zstyle ':completion:*:hosts' hosts $_hosts
fi
