[ ! -x "$(which helm)" ] && return

source <(helm completion zsh)
