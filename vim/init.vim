"
" Global
"
runtime! debian.vim

"
" Plugins
"
call plug#begin('~/.local/share/nvim/site/plugged/')
" Apearance plugins
Plug 'chriskempson/base16-vim'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Helper plugins
Plug 'SirVer/ultisnips'
Plug 'ciaranm/detectindent'
Plug 'honza/vim-snippets'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'

" Syntax plugins
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
Plug 'rodjek/vim-puppet', { 'for': 'puppet' }
Plug 'vim-latex/vim-latex', { 'for': 'tex' }
Plug 'vim-syntastic/syntastic'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-jedi', { 'for': 'python' }
Plug 'davidhalter/jedi-vim', { 'for': 'python' }
call plug#end()

let g:airline_powerline_fonts = 1

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#tab_min_count = 2
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#show_tab_nr = 0
let g:airline#extensions#tabline#show_tab_type = 0
let g:airline#extensions#tabline#show_close_button = 0

let g:deoplete#enable_at_startup = 1
let g:deoplete#disable_auto_complete = 1
autocmd FileType python,puppet let g:deoplete#disable_auto_complete = 0

let g:detectindent_preferred_expandtab = 1
let g:detectindent_preferred_indent = 2

let g:syntastic_aggregate_errors = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_check_on_open = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_puppet_puppetlint_args = "--no-autoloader_layout-check"
let g:syntastic_python_checkers = ['python', 'flake8', 'pydocstyle']
let g:syntastic_python_python_exec = '/usr/bin/python3'
let g:syntastic_python_flake8_post_args="--max-line-length=88"

let g:Tex_SmartKeyQuote = 0
let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_CompileRule_pdf = 'xelatex --interaction=nonstopmode --output-directory=output $*'
let g:Tex_ViewRule_pdf = 'evince'

"
" Interface
"
set t_Co=256
set background=dark

let base16colorspace=256
colorscheme base16-gruvbox-dark-pale

set lazyredraw

set number
set scrolloff=5

set linebreak
set colorcolumn=80

set shortmess=at

set spelllang=fr

highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

"
" Key behavior
"
set mouse=
set nostartofline
set pastetoggle=<F10>
map <silent> <C-L> :let @/ = ""<CR>
map <silent> tt :tabnext<CR>
map <silent> ty :tabprev<CR>
map tn :tabnew<Space>

noremap <ESC>[1;5D <C-Left>
noremap! <ESC>[1;5D <C-Left>
noremap <ESC>[1;5C <C-Right>
noremap! <ESC>[1;5C <C-Right>

"
" Indentation
"
set expandtab
set softtabstop=2
set shiftwidth=2

"
" Completion
"
set infercase
set completeopt=menuone,preview

"
" Folding
"
set foldmethod=syntax
set foldlevel=99

"
" Search
"
set showmatch
set ignorecase

"
" Files and filetypes
"
set fileencoding=utf-8

" Remove whitespace before writing to disk
autocmd BufWritePre * :%s/\s\+$//e

autocmd BufNewFile,BufRead *.tex :setlocal spell
autocmd BufNewFile,BufRead *.py :set expandtab tabstop=4 shiftwidth=4
autocmd FileType gitcommit :set textwidth=71 colorcolumn=72

let g:tex_flavor = "latex"

augroup gzip
  autocmd!
  autocmd BufReadPre,FileReadPre *.gz,*.Z,*.bz2 set bin
  autocmd BufReadPost,FileReadPost *.gz,*.Z '[,']!gunzip
  autocmd BufReadPost,FileReadPost *.bz2 '[,']!bunzip2
  autocmd BufReadPost,FileReadPost *.gz,*.Z,*.bz2 set nobin
  autocmd BufReadPost,FileReadPost *.gz,*.Z,*.bz2 execute ":doautocmd BufReadPost " . expand("%:r")
  autocmd BufWritePost,FileWritePost *.gz,*.Z,*.bz2 !mv <afile> <afile>:r
  autocmd BufWritePost,FileWritePost *.gz !gzip <afile>:r
  autocmd BufWritePost,FileWritePost *.bz2 !bzip2 <afile>:r
  autocmd BufWritePost,FileWritePost *.Z !compress <afile>:r
  autocmd FileAppendPre *.gz,*.Z !gunzip <afile>
  autocmd FileAppendPre *.bz2 !bunzip2 <afile>
  autocmd FileAppendPre *.gz,*.Z,*.bz2 !mv <afile>:r <afile>
  autocmd FileAppendPost *.gz,*.Z,*.bz2 !mv <afile> <afile>:r
  autocmd FileAppendPost *.gz !gzip <afile>:r
  autocmd FileAppendPost *.bz2 !bzip2 <afile>:r
  autocmd FileAppendPost *.Z !compress <afile>:r
augroup END
